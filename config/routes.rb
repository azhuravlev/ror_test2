Rails.application.routes.draw do
  devise_for :users

  devise_scope :user do
    get 'users/account', to: 'users#show', as: :account
    post 'users/add_device', as: :add_device

    authenticated :user do
      root 'users#show', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  resources :devices, only: [:create, :update] do
    collection do
      get 'check_activation', as: :check_activation
    end
  end

  namespace :api do
    namespace :v1 do
      get 'tests', to: 'test#index'
    end
  end
end
