require 'spec_helper'

describe Api::V1::TestController do
  describe "index" do
    let!(:user) { create :user }
    let(:device) { create :device, user: user, active: true }

    it 'return authorized: "OK" if user authorized' do
      @request.headers["HTTP_X_DEVICE_MAC"] = device.mac
      @request.headers["HTTP_X_DEVICE_TOKEN"] = device.create_and_return_token
      get :index
      expect(response.status).to be(200)
      expect(response.body).to include('OK')
    end

    it 'return error if user not authorized' do
      get :index
      expect(response.status).to be(401)
      expect(response.body).to include('Not authorized')
    end
  end
end