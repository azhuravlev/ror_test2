require 'spec_helper'

describe DevicesController do
  it "should create device with valid params an respond with activation by code" do
    expect {
      post :create, valid_device_params
    }.to change(Device, :count).by(1)
    expect(response.code).to eql '200'
    expect(response.body).to include('activation_code')
  end

  it "should not create device with valid params an respond with activation by code" do
    expect {
      post :create, device: { model: 'TestModel', mac: '123'}
    }.to_not change(Device, :count)
    expect(response.code).to eql '400'
    expect(response.body).to_not include('activation_code')
  end

  context 'check_activation' do
    let(:user) { create :user }
    let(:unactivated_device) { create :device }
    let(:activated_device) { create :device, active: true, user: user }

    it "should return 404 for unknown device" do
      get :check_activation, valid_device_params
      expect(response.code).to eql '404'
      expect(response.body).to include('unknown')
    end

    it "should return 200 and activation status for known device" do
      Device.stub(:find_by).and_return(unactivated_device)
      get :check_activation, device: { model: unactivated_device.model, mac: unactivated_device.mac }
      expect(response.code).to eql '200'
      expect(response.body).to include('unactivated')
    end

    it "should return 200 and activation status for known device" do
      Device.stub(:find_by).and_return(activated_device)
      get :check_activation, device: { model: activated_device.model, mac: activated_device.mac }
      expect(response.code).to eql '200'
      expect(response.body).to include('authentication_token')
    end
  end
end

def valid_device_params
  {
    device: {
      model: 'MyDevice',
      mac: '01:02:03:04:15:33'
    }
  }
end
