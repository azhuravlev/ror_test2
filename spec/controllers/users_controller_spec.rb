require 'spec_helper'

describe UsersController do
  before do
    user = create :user
    request.env['warden'].stub :authenticate! => user
    controller.stub current_user: user
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  it "should activate present device by code" do
    device = create :device
    post :add_device, code: device.activation_code
    expect(device.reload.active).to eq(true)
  end

  it "should not activate any device if not foun" do
    post :add_device, code: 123
    expect(Device.where(active: true)).to be_empty
  end
end
