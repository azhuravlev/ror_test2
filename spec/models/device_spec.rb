require 'spec_helper'

describe Device do
  it "should set activation code on create" do
    device = Device.new mac: '01:02:03:04:ab:cd', model: 'SomeModel'
    expect(device).to be_valid
    expect(device.activation_code).not_to be
    device.save
    expect(device.activation_code).to be
  end

  it "should validate mac adress format" do
    device = Device.new model: 'SomeModel', mac: '123'
    expect(device).not_to be_valid
    device.mac = '01:02:03:04:ab:cd'
    expect(device).to be_valid
  end

  it "should validate mac adress uniqueness" do
    fdev = create :device, mac: '01:02:03:04:ab:cd'
    device = Device.new model: 'SomeModel', mac: '01:02:03:04:ab:cd'
    expect(device).not_to be_valid
  end

  context "auth token generation" do
    let!(:user) { create :user }

    it "should generate auth token for user and return it if active, and reset activation code" do
      device = create :device, user: user, active: true
      token = device.create_and_return_token
      expect(token).not_to be_nil
      expect(device.authentication_token).not_to be_nil
      expect(device.activation_code).to be_nil
    end

    it "shouldn't generate auth token unless active" do
      device = create :device, user: user, active: false
      token = device.create_and_return_token
      expect(token).to be_nil
      expect(device.authentication_token).to be_nil
      expect(device.activation_code).not_to be_nil
    end

    it "shouldn't generate auth token unless user assigned" do
      device = create :device, active: true
      token = device.create_and_return_token
      expect(token).to be_nil
      expect(device.authentication_token).to be_nil
      expect(device.activation_code).not_to be_nil
    end
  end
end
