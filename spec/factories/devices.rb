FactoryGirl.define do
  factory :device do
    activation_code nil
    model "MyString"
    mac "01:02:03:04:ab:cd"
    user nil
    authentication_token nil
  end
end
