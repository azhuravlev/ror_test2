class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.integer :activation_code
      t.string :model
      t.string :mac
      t.references :user, index: true
      t.references :authentication_token, index: true

      t.timestamps null: false
    end
    add_foreign_key :devices, :users
    add_foreign_key :devices, :authentication_tokens
  end
end
