class AddTokenInfoToDevice < ActiveRecord::Migration
  def change
    add_column :devices, :authentication_token, :string
    add_column :devices, :last_used_at, :datetime
  end
end
