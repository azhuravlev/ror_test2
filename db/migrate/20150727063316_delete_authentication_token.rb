class DeleteAuthenticationToken < ActiveRecord::Migration
  def up
    drop_table :authentication_tokens
  end
end
