class DevicesController < ApplicationController
  protect_from_forgery

  def create
    device = Device.new device_params
    if device.save
      render json: { activation_code: device.activation_code }, status: 200
    else
      render json: device.errors, status: 400
    end
  end

  def check_activation
    device = Device.find_by device_params
    if device
      token = device.create_and_return_token
      if token
        render json: { authentication_token: token }, status: 200
      else
        render json: { device: 'unactivated' }, status: 200
      end
    else
      render json: { device: 'unknown' }, status: 404
    end
  end

  private

  def device_params
    params.require(:device).permit(:mac, :model)
  end
end