class UsersController < Devise::RegistrationsController
  before_action :require_current_user, only: [:show, :add_device]

  def show
    @devices = current_user.devices
  end

  def add_device
    code = params[:code].to_i
    device = Device.find_by activation_code: code
    if device
      device.activate_for(current_user)
    end
    redirect_to account_path
  end
end