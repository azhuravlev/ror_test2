module Api
  module V1
    class BaseController < ActionController::Base
      layout false
      protect_from_forgery
      before_action :authenticate_user_from_token!

      before_action :set_default_response_format
      rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

      private

      def authenticate_user_from_token!
        device = Device.find_by mac: mac_from_headers
        user = device.try(:user)
        return render_unauthorized unless user

        if device.check_token(token_from_headers)
          touch_device(device)
          sign_in user, store: false
        else
          render_unauthorized
        end
      end

      def render_unauthorized
        self.headers['WWW-Authenticate'] = 'Token realm="Application"'
        render json: 'Not authorized', status: 401
      end

      def set_default_response_format
        request.format = :json
      end

      def mac_from_headers
        request.headers["HTTP_X_DEVICE_MAC"]
      end

      def token_from_headers
        request.headers["HTTP_X_DEVICE_TOKEN"]
      end

      def touch_device(device)
        device.update_attribute(:last_used_at, DateTime.current) if device.last_used_at.nil? || device.last_used_at < 1.hour.ago
      end
    end
  end
end

