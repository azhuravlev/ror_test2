module Api
  module V1
    class TestController < Api::V1::BaseController
      def index
        render json: { authorized: 'OK' }, status: 200
      end
    end
  end
end