class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def require_current_user
    return if current_user
    if request.xhr?
      render status: 403, text: 'Authentication required'
    else
      redirect_to new_user_session_path
    end
  end
end
