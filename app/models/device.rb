class Device < ActiveRecord::Base
  belongs_to :user

  before_create :reset_activation_code

  validates :mac, :model, presence: true
  validates :mac, format: { with: /\A([0-9a-fA-F]{2}[:-]){5}[0-9a-fA-F]{2}\z/i }, uniqueness: true, on: :create

  CODE_LENGTH = 6

  def activate_for(user)
    self.user = user
    self.active = true
    save!
  end

  def check_token(token_from_headers)
    token_body = Devise.token_generator.digest(Device, :authentication_token, token_from_headers)
    Devise.secure_compare(authentication_token, token_body)
  end

  def create_and_return_token
    return unless active && user

    token, token_body = Devise.token_generator.generate(Device, :authentication_token)

    self.authentication_token = token_body
    self.activation_code = nil
    self.save!

    token
  end

  private

  def reset_activation_code
    self.activation_code = generate_activation_code
  end

  def generate_activation_code
    mult = 10 ** CODE_LENGTH
    (SecureRandom.random_number * mult).ceil
  end
end
